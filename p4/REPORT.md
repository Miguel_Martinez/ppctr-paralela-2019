# P4: Paralelización de I/O en OpenMP
Miguel Martinez Diaz

Fecha 18/12/2019

## Prefacio

En esta practica se ha logrado paralelizar un código secuencial utilizando técnicas de la programación empleando para ello la API OpenMP (de ahora en adelante, OMP), esta práctica muestra las ventajas y riesgos que tiene la paralelización de código sobre una sección de código de Entrada/salida (E/S; I/O)

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

- Ejecución sobre Dispositivo Apple MacBook Air Mid 2013:
  - macOS 10.14.5 (Mojave)
  - Con entorno gráfico
  - CPU Intel core i5 4250U
  - CPU 2 cores: 1 socket, 2 threads/core, 2 cores/socket.
  - CPU @ 1300 MHz
  - CPU: L2: 256KB/core, L3: 3MB/total
  - Mas información en: https://ark.intel.com/content/www/es/es/ark/products/75028/intel-core-i5-4250u-processor-3m-cache-up-to-2-60-ghz.html
  - 4 GB RAM @ 1600MHz
  - Trabajo y benchmarks sobre SSD
  - Sin entorno gráfico en foreground durante ejecuciones

## 2. Diseño e Implementación del Software

El software desarrollado para esta práctica consiste en al modificación del fichero video_task.c, el cual, en su función principal lee, procesa y escribe un archivo generado por el código del archivo generator.c. La solución encontrada a este problema consiste en levantar todos los hilos posibles *#pragma omp parallel*, hacer que un hilo maestro *#pragma omp master* se ocupe de la parte de lectura de un fichero, mientas que se generan tareas que procesan y reescriben el archivo que van siendo asignadas a los hilos que quedan libres, esto se logra mediante la directiva *#pragma omp task shared(filtered, pixels, in, out) if(size)* todo esto se realiza dentro del bucle do-while que al final tiene la directiva *#pragma omp taskwait* para sincronizar las tareas y evitar escrituras fuera de orden y posibles desordenes en la escritura de las imágenes.



## 3. Metodología y desarrollo de las pruebas realizadas

Las pruebas realizadas consisten en la ejecución del ejecutable, y comprobar que cada hilo tiene asignada una tarea, así como ver que se espera a que se sincronizen correctamente, en este equipo las tareas se asignaban y ejecutaban a los 4 hilos disponibles, salvo por las ultimas 2 iteraciones.

Para comprobar el código se compiló previamente la versión secuencial y se almacenó su resultado en un fichero, posteriormente se compiló la versión paralela y el fichero que genera esta versión es comprobado con el fichero secuencial con el comando *diff archivo archivo_seq* En este caso, y debido a errores desconocidos, el comando nos dice que los ficheros difieren en contenido.

También se comprueban los tiempos de la misma manera que en al práctica 1 y 2, dando como resultado un tiempo en secuencial de unos 10 segundos y en paralelo de unos 5 segundos.

## 4. Discusión

 Esta práctica muestra las ventajas que ofrece la programación paralela, así como su sencillez al emplear OMP, en este aso está muy centrada en la creación de tareas, las cuales son asíncronas, es decir, de duración indefinida, por lo que su sincronización puede ser una parte crítica de la programación, asunto resuelto de una manera muy simple con OMP.
