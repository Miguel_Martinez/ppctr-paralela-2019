# P1: Multithreading en C++
Miguel Martinez Diaz

Fecha 27/11/2019

## Prefacio

Objetivos conseguidos

Breve descripción de lo que te ha parecido la práctica. Aprendizajes, dificultades, cosas interesantes, qué te hubiera gustado hacer o mejorarías, qué conceptos te gustaría haber practicado de C++/OMP, opinión sobre el informe, etc

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

- Ejecución sobre Dispositivo Apple MacBook Air Mid 2013:
  - macOS 10.14.5 (Mojave)
  - Con entorno gráfico
  - CPU Intel core i5 4250U
  - CPU 2 cores: 1 socket, 2 threads/core, 2 cores/socket.
  - CPU @ 1300 MHz
  - CPU: L2: 256KB/core, L3: 3MB/total
  - Mas información en: https://ark.intel.com/content/www/es/es/ark/products/75028/intel-core-i5-4250u-processor-3m-cache-up-to-2-60-ghz.html
  - 4 GB RAM @ 1600MHz
  - Trabajo y benchmarks sobre SSD
  - Sin entorno gráfico en foreground durante ejecuciones

## 2. Diseño e Implementación del Software

El software consiste solamente en un archivo, en el cual se define una clase principal, la cual recibe los parámetros solicitados.
a continuación se almacena el tiempo actual antes de la ejecución, se inicializa el array con valores aleatorios,
se define el numero de hilos entre 1 y 12(como se solicita), se asigna un indice para cada una de las posibles operaciones a ejecutar,
se crean tantos hilos como sean necesarios y se les asigna a todos ellos la función selectOp, la cual, en base al indice de operación,
realizará una operación u  otra con los valores aportados(indice de operación, puntero al primer valor del array a procesar,
numero de elementos a procesar y puntero al valor de resultado)

A continuación, cada hilo ejecuta independientemente el mismo código a la vez en base a la operación solicitada(se define una función para cada operación,
todas ellas reciben los mismos valores que la función de seleccionar operación, a excepción del índice de operación), y a la hora de escribir el resultado
en la variable global resultado, es necesario tomar un mutex común a todos los hilos, con el fin de prevenir accesos simultáneos a este valor. Una vez terminada
esta fase, los hilos compiten por tomar un segundo mutex y, una vez tomado, si la variable bLastExecution es false, ese hilo vuelve a ejecutar el mismo código con la parte restante del array, finalmente, tras ejecutar o no el código, libera otra vez el mutex.

Finalmente, tras crear todos los hilos, se van eliminando a medida que van terminando, se captura el tiempo actual en ese momento, se resta con el tiempo de inicio y finaliza el programa.

## 3. Metodología y desarrollo de las pruebas realizadas

Las pruebas realizadas consisten en la ejecución de 11 repeticiones del programa, para cada operación, de 1 a 4 threads (máximo disponible de la CPU) sobre un array de 400.000.000 elementos.

Se calcula el tiempo medio de cada ejecución con las ejecuciones 1 a 10, ignorándose la ejecución 0 por ser de calentamiento

El [script ejecutable](p1/Anexos/script.sh) se encuentra en una carpeta llamada "Anexos"

Se muestra una gráfica representando el tiempo medio empleado (Eje X, unidades en segundos) empleado por cada ejecución con cada número de threads (Eje Y)
![Imagen de la grafica](p1/Anexos/Resutados_Ejecuciones.png)


También se añade en la misma carpeta un [documento CSV](p1/Anexos/Resutados_Ejecuciones.csv) con los resultados totales de las ejecuciones consideradas.


## 4. Discusión

Esta práctica ofrece una simple, pero efectiva forma de implementar y visualizar los beneficios de la programación paralela en un caso básico, aunque muy comúm en la computación de nuestros dias, especialmente en campos como los de gráficos, sin embargo, otras partes de la práctica, como la implementación de un hilo logger, no se le encuentra una relación suficiente con la programación paralela como para ser considerada en esta práctica.


## 5. Propuestas optativas

### Propuesta 1

Se propone, para cursos próximos, realizar una práctica1 mas simple, limitándolo a la paralelización del cómputo de un array o un vector, básicamente, la parte básica de esta práctica, y dejar para parte opcional/extra el conector con java, así como adecuar el peso de esta práctica en la asignatura a la nueva dificultad, y, en caso de considerarse necesario, implementar el hilo logger en una práctica dedicada en exclusiva a la compilación condicional.

Se defiende que realizar una primera práctica tan larga y compleja puede desmotivar al alumnado, dando la impresión de que la parte de Paralela de la asignatura es mas compleja de lo que puede ser en realidad.


## Anexos

**NOTA 1**: En lugar de inicializar el array con el indice de la posición en formato long, se ha optado por inicializarlo con valores aleatorios, ya que se considera que esto supone un caso mas realista, ya que se incluyen valores decimales, que con índices no estarían disponibles.
