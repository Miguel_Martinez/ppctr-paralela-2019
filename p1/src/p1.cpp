#include <stdio.h>
#include <thread>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstdlib>
#include <mutex>
#include <sys/time.h>

void createArray(int array_length, double* values);
void selectop(int intOp, double* first_value, int work_size, double* result, bool* bLastExecution, double* values, int array_length, int num_threads);
void add(double* first_value, int work_size, double* result);
void fxor(double* first_value, int work_size, double* result);
void sub(double* first_value, int work_size, double* result);
std::mutex g_mtx;
std::mutex first_mtx;


int main(int argc, char *argv[]){
	srand(time(NULL));
	std::cout << "/* Inicia el programa */" << '\n';
	int array_length;
	int intOp;
	int num_threads;
	double* values;
	int work_size;
	double result = 0;
	timeval listaTiempos[2];
	bool bLastExecution=false;

	#ifdef FEATURE_LOGGER
	std::cout << "/* Logger Activado (no hay nada implementado) */" << '\n';
	#endif

	std::cout << "/* asigna variables */" << '\n';

	std::cout << argc << argv[0] << argv[1] << argv[2] << '\n';

	if(argc<3){
		printf("Error en el numero de parámetros\n");
		exit(-1);
	}else if(argc == 3){
		array_length = atoi(argv[1]);
		num_threads = 1;
	}else if(argc == 4){
		array_length = atoi(argv[1]);
		num_threads = atoi(argv[3]);
	}



	values = (double*)malloc(array_length * sizeof(double));

	std::cout << "/* espacio del array reservado en memoria */" << '\n';


	if(num_threads<1){
		num_threads=1;
	}else if(num_threads>12){
		num_threads=12;
	}

	createArray(array_length, values);
	work_size = array_length/num_threads;

	std::cout << "/* array Creado */" << '\n';


	std::cout << "/* asigna indices operacion */" << '\n';
	std::cout << argv[2] << '\n';

	if(strcmp(argv[2], "add") == 0){
		intOp=0;
	}else if(strcmp(argv[2], "xor") == 0){
		intOp=1;
	}else if(strcmp(argv[2], "sub") == 0){
		intOp=2;
	}else{
		intOp=-1;
	}



	std::cout << "/* previo creación hilos */" << '\n';
	std::thread threads[num_threads];
	gettimeofday(&listaTiempos[0], NULL);
	std::cout << "tiempo inicio:" << listaTiempos[0].tv_sec << "s   "  << listaTiempos[0].tv_usec << "ms" << '\n';

	for(int i=0; i<num_threads; ++i){
		double *first_value = &values[i*work_size];
		threads[i] = std::thread (selectop, intOp, first_value, work_size, &result, &bLastExecution, values, array_length, num_threads);
	}
	std::cout << "/* hilos creados, a destruir */" << '\n';
	//destroy threads
	for(int i=0; i<num_threads; ++i){
		threads[i].join();
	}
	gettimeofday(&listaTiempos[1], NULL);
	long secondsToMicroseconds = (listaTiempos[1].tv_sec-listaTiempos[0].tv_sec)*1000000;
	long totalTimeInMicroseconds = secondsToMicroseconds + (listaTiempos[1].tv_usec-listaTiempos[0].tv_usec);
	float totalTimeInSeconds = (float)(totalTimeInMicroseconds)/1000000;
	std::cout << "tiempo final:" << listaTiempos[1].tv_sec << "s   "  << listaTiempos[1].tv_usec << "ms" << '\n';
	std::cout << "tiempo transcurrido en microsegunos:" << totalTimeInMicroseconds << '\n';
	std::cout << "tiempo transcurrido en segundos :" << totalTimeInSeconds << '\n';

	std::cout << "Resultado:  " << result << '\n';
	std::cout << "/* hilos destruidos, cierra programa */" << '\n';
	free(values);
	exit(0);
}




void createArray(int array_length, double* values){
	for(int i=0; i<array_length; i++){
		values[i]=rand()/100;
	}
}

void selectop(int intOp, double* first_value, int work_size, double* result, bool* bLastExecution, double* values, int array_length, int num_threads){
	if(intOp==0){
		add(first_value, work_size, result);
	}else if(intOp==1){
		fxor(first_value, work_size, result);
	}else if(intOp==2){
		sub(first_value, work_size, result);
	}else{
		std::cout << "/* operacion no reconocida */" << '\n';
	}
	if(array_length>num_threads*work_size){
		first_mtx.lock();
		if(!*bLastExecution){
			std::cout <<  "Entra en el ultimo hilo2------------------------------" << '\n';
			*bLastExecution=true;
			*first_value = values[num_threads*work_size];
			work_size = array_length - num_threads*work_size;
			if(intOp==0){
				add(first_value, work_size, result);
			}else if(intOp==1){
				fxor(first_value, work_size, result);
			}else if(intOp==2){
				sub(first_value, work_size, result);
			}else{
				std::cout << "/* operacion no reconocida */" << '\n';
			}
		}
		first_mtx.unlock();
	}
}

void add(double* first_value, int work_size, double* result) {
	double localResult=0;
	for(int i=0; i<work_size; i++){
		localResult += first_value[i];
	}
	g_mtx.lock(); //lock_guard
	*result+=localResult;
	std::cout << *result << '\n';
	g_mtx.unlock();
}

void fxor(double* first_value, int work_size, double* result) {
	long localResult=0;
	for(int i=0; i<work_size; i++){
		localResult ^= (long)first_value[i];
	}
	g_mtx.lock();
	*result=(double)((long)*result^localResult);
	std::cout << *result << '\n';
	g_mtx.unlock();
}

void sub(double* first_value, int work_size, double* result) {
	double localResult=0;
	for(int i=0; i<work_size; i++){
		localResult -= first_value[i];
	}
	g_mtx.lock();
	*result+=localResult;
	std::cout << *result << '\n';
	g_mtx.unlock();
}
