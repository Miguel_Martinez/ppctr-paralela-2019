its=100000000
FILE=values.csv
newline="\n"
for i in $(seq 0 10);
do
  echo
  echo
  echo
  echo
  echo "ejecución $i secuencial"
  ./seq $its
done
printf '\n'>>$FILE

for j in $(seq 1 4);
do
  for i in $(seq 0 10);
  do
    echo
    echo
    echo
    echo
    echo "ejecución $i c++ $j threads"
    ./cpp $its $j
  done
  printf '\n'>>$FILE

  for i in $(seq 0 10);
  do
    echo
    echo
    echo
    echo
    echo "ejecución $i openMP $j threads"
    ./omp $its $j
  done
  printf '\n'>>$FILE
done
