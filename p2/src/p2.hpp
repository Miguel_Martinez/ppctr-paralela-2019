#ifndef P2_HPP
#define P2_HPP

#include <stdio.h>
#include <math.h>
#include <thread>
#include <cstdlib>
#include <omp.h>
#include <iostream>
#include <sys/time.h>


std::mutex g_mtx;
timeval listaTiempos[2];

struct Aggregator {
  double separator;
  double A;
  double B;
  double C;
};

struct Data {
  double value;
  double gap;
  Aggregator* agg;
  long numIts;
};

void sequential(Data data);
void cpp(Data data);
void omp(Data data);

Data parse(long numIts);
int attach(Data* data, long iterations);

void operation(int index, int thread_numIts, double *tmp, Data *data);
#endif // P2_HPP
