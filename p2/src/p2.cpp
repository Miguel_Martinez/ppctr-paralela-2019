#include "p2.hpp"

long numIts=0;
int numThreads=0;
//time listaTiempos[2];

int main(int argc, char *argv[]){

  if (argc ==2){
    numIts=atoi(argv[1]);
  }else if(argc==3){
    numIts=atoi(argv[1]);
    numThreads=atoi(argv[2]);
  }

  Data data = parse(numIts);

  gettimeofday(&listaTiempos[0], NULL);
  #ifdef SEQUENTIAL
  std::cout << "Codigo secuencial" << '\n';
    sequential(data);
  #endif

  #ifdef PARALLELL
  std::cout << "Codigo paralelo" << '\n';
  if(numThreads==0){
    std::cout << "No threads requested." << '\n';
    exit(-1);
  }
    cpp(data);
  #endif

  #ifdef OPENMP
  std::cout << "Codigo OpenMP" << '\n';
  if(numThreads==0){
    std::cout << "No threads requested." << '\n';
    exit(-1);
  }
    omp(data);
  #endif
  gettimeofday(&listaTiempos[1], NULL);
  long secondsToMicroseconds = (listaTiempos[1].tv_sec-listaTiempos[0].tv_sec)*1000000;
	long totalTimeInMicroseconds = secondsToMicroseconds + (listaTiempos[1].tv_usec-listaTiempos[0].tv_usec);
	float totalTimeInSeconds = (float)(totalTimeInMicroseconds)/1000000;
	std::cout << "tiempo transcurrido en segundos :" << totalTimeInSeconds << '\n';

  FILE *fp;
  fp = fopen( "values.csv" , "a" );
  fprintf(fp, "%f, ", totalTimeInSeconds);

}

int attach(Data* data, long iterations){
  if (iterations <= 0){
    return -1;
  }
  Aggregator* agg = (Aggregator*)malloc(sizeof(Aggregator));
  agg->separator = (double)1/(double)iterations;
  agg->A = 1.5;
  agg->B = 2.4;
  agg->C = 1.0;
  data->agg = agg;
  return 0;
}

Data parse(long numIts){
  // parse 'long' and set numIts
  Data d;
  d.numIts = numIts;
  d.gap = 1.0/(double)d.numIts;
  d.value = 0.0;
  if (attach(&d, numIts) != 0){
    fprintf(stderr, "error: invalid iterations\n");
    exit(-1);
  };
  return d;
}

#ifdef SEQUENTIAL
void sequential(Data data){
  int i;
  int jump = 2.4;
  double value;
  double tmp = 0;
  double ellipse;

  int id;
  double x;
  for (i=0; i<data.numIts; i=i+1) {
    x = (i+(double)1/(int)2.0) * data.gap;
    tmp = tmp + pow(2,2)/(1+pow(x, 2));
  }
  value = tmp * data.agg->separator;
  ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

  printf("ellipse: %f \n", ellipse);
}
#endif

#ifdef PARALLELL
void cpp(Data data){
  int i;
  int jump = 2.4;
  double value;
  double tmp = 0;
  double ellipse;


  int id, offset, thread_numIts;
  std::thread threads[numThreads];
  thread_numIts = (int)(numIts/numThreads);
  for (i=0; i<numThreads; i++) {
    threads[i] = std::thread (operation, i, thread_numIts, &tmp, &data);
  }

  for (i=0; i<numThreads; i++) {
    threads[i].join();
  }

  value = tmp * data.agg->separator;
  ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

  printf("ellipse: %f \n", ellipse);
}

void operation(int index, int thread_numIts, double *tmp, Data *data){
  double local_x, local_tmp;
  if(index==numThreads-1){
    for (int i=index*thread_numIts; i<numIts; i++) {
      local_x = (i+(double)1/(int)2.0) * data->gap;
      local_tmp = local_tmp + pow(2,2)/(1+pow(local_x, 2));
    }
  }else{
    for (int i=index*thread_numIts; i<thread_numIts+index*thread_numIts; i++) {
      local_x = (i+(double)1/(int)2.0) * data->gap;
      local_tmp = local_tmp + pow(2,2)/(1+pow(local_x, 2));
    }
  }
  g_mtx.lock();
  *tmp = *tmp + local_tmp;
  g_mtx.unlock();
}
#endif

#ifdef OPENMP
void omp(Data data){
  int i;
  int jump = 2.4;
  double value;
  double tmp = 0;
  double ellipse;

  int id;
  double x;


  int thread_numIts = (int)(numIts/numThreads);
  #pragma omp parallel num_threads(numThreads) shared(thread_numIts, tmp, data) private(x, i)
  {
    double local_tmp, local_x;
    int index = omp_get_thread_num();
    if(index==numThreads-1){
      for (int i=index*thread_numIts; i<numIts; i++) {
        local_x = (i+(double)1/(int)2.0) * data.gap;
        local_tmp = local_tmp + pow(2,2)/(1+pow(local_x, 2));
      }
    }else{
      for (int i=index*thread_numIts; i<thread_numIts+index*thread_numIts; i++) {
        local_x = (i+(double)1/(int)2.0) * data.gap;
        local_tmp = local_tmp + pow(2,2)/(1+pow(local_x, 2));
      }
    }
    tmp=tmp+local_tmp;
  }
  value = tmp * data.agg->separator;
  ellipse = 2 * value / sqrt((4 * data.agg->A * data.agg->C - pow(data.agg->B, 2)));

  printf("ellipse: %f \n", ellipse);
}
#endif
