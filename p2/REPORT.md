# P2: Paralelización en C++ y OpenMP
Miguel Martinez Diaz

Fecha 18/12/2019

## Prefacio

En esta practica se ha logrado paralelizar un código secuencial utilizando técnicas de la programación en C++, así como empleando la API OpenMP (de ahora en adelante, OMP), esta práctica muestra las ventajas y riesgos que tiene la paralelización de código, así como una implementación mas simple con OMP frente a Código "puro" C++.

También se logra crear varios ejecutables partiendo de un único archivo fuente mediante compilación condicional.
## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

- Ejecución sobre Dispositivo Apple MacBook Air Mid 2013:
  - macOS 10.14.5 (Mojave)
  - Con entorno gráfico
  - CPU Intel core i5 4250U
  - CPU 2 cores: 1 socket, 2 threads/core, 2 cores/socket.
  - CPU @ 1300 MHz
  - CPU: L2: 256KB/core, L3: 3MB/total
  - Mas información en: https://ark.intel.com/content/www/es/es/ark/products/75028/intel-core-i5-4250u-processor-3m-cache-up-to-2-60-ghz.html
  - 4 GB RAM @ 1600MHz
  - Trabajo y benchmarks sobre SSD
  - Sin entorno gráfico en foreground durante ejecuciones

## 2. Diseño e Implementación del Software

El software consiste en dos ficheros: p2.hpp, el cual contiene la inclusión de todas las librerías, estructuras y declaración de los elementos necesarios para la práctica. Y p2.cpp el cual contiene una clase principal que determinan el numero de iteraciones, y, en caso de ser necesario, el numero de hilos que levantar, y la llamada a cada método en función de si estamos compilando código secuencial o paralelo con c++ u OMP, mediante compilación condicional. antes y después de la llamada al método que resuelve el problema, guarda la hora local, para al final de todo mostrar por pantalla el tiempo total de ejecución.

el método secuencial viene ya aportado en el repositorio, hemos creado, para la parte de paralelización "pura" dos métodos:
  - cpp(Data data): El cual levanta el numero de hilos solicitado y tras estos terminar de ejecutar su código, los elimina.
  - operation(int index, int thread_numIts, double \*tmp, Data \*data): Es el codigo que ejecuta cada hilo, todos los hilos ejecutan una parte proporcional de las iteraciones(thread_numIts), salvo el último, que ejecuta su parte proporcional junto con las iteraciones restantes. Cada hilo computa las variables x y tmp (que en secuencial eran globales) de forma local, y, cuando termian de computar sus iteraciones correspondientes, lucha por obtener un mutex que le dará acceso a sobreescribir la variable global \*tmp.

y para OMP solo uno, el cual parte del código secuencial:
  - omp(Data data): Sigue el mismo funcionamiento que el codigo "puro", aunque esta vez no hace falta crear los hilos explícitamente y una función que les haga trabajar, basta poner la directiva   *#pragma omp parallel num_threads(numThreads) shared(thread_numIts, tmp, data) private(x, i)* para decirle cuantos hilos hay que levantar *num_threads(num_Threads)*, que datos son compartidos *shared(thread_numIts, tmp, data)* y cuales propios a cada hilo *private(x, i)*



## 3. Metodología y desarrollo de las pruebas realizadas

Las pruebas realizadas consisten en la ejecución de 11 repeticiones del programa, para cada versión, de 1 a 4 threads (máximo disponible de la CPU, a excepción de la versión secuencial) con 100.000.000 iteraciones.

Se calcula el tiempo medio de cada ejecución con las ejecuciones 1 a 10, ignorándose la ejecución 0 por ser de calentamiento

El [script ejecutable](p2/Anexos/script.sh) se encuentra en una carpeta llamada "Anexos"

Se muestra una gráfica representando el tiempo medio empleado (Eje X, unidades en segundos) empleado por cada ejecución con cada número de threads (Eje Y)
![Imagen de la grafica](p2/Anexos/valuesP2.png)

También se añade en la misma carpeta un [documento CSV](p2/Anexos/valuesP2.csv) con los resultados totales de las ejecuciones consideradas.

Los resultados son claros a la hora de mostrar la ganancia en tiempo a la hora de aprovechar las arquitecturas multihilo disponibles en los computadores a día de hoy, así como una gran similitud entre la programación paralela "pura" y usar OMP.

## 4. Discusión

 Esta práctica muestra las ventajas que ofrece la programación paralela, así como su sencillez al emplear OMP, esto es útil a la hora de perder el miedo a la hora de implementar nuestros proyectos de forma mas eficiente en una arquitectura multihilo, al añadir una gran capa de abstracción entre el programador y la arquitectura y programación multihilo.
